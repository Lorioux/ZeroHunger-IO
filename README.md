# ZeroHunger.IO

Zero  Hunger platform to support food and commodities managements for wast management. People registers to Zero Hunger 
Co-Ops and are notified for food availablity. Smallholder farmers register to Zero Hunger Co-Ops, so they can share their products images for sell.

## Notification service:

Smallholders with handset are notified via SMS push to follow a hyperlink iin order to upload product images. Images are taken using a camera or an embeded no-smartphone camera. Product images are send to Co-Ops Management Platform for Quality Evaluation.

## Co-Ops Management:

Co-ops manager register both smallholder farmers and buyers into the platform. Smallholder farmers provide information related to farmed area, crops, location, etc.

